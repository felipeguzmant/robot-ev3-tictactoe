#pragma config(Sensor, S1,     luz,            sensorEV3_Color, modeEV3Color_Color)
#pragma config(Motor,  motorA,          izquierda,     tmotorEV3_Large, PIDControl, driveLeft, encoder)
#pragma config(Motor,  motorC,          lapiz,         tmotorEV3_Medium, PIDControl, driveLeft, encoder)
#pragma config(Motor,  motorD,          derecha,       tmotorEV3_Large, PIDControl, driveRight, encoder)


void avanzar(int power_motor){
    motor[motorA]=power_motor;
    motor[motorD]=power_motor;
    wait1Msec(500);
}


task main()
{
	avanzar(15);
}